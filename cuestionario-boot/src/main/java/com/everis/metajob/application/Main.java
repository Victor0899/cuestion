package com.everis.metajob.application;

import org.terasoluna.plus.config.boot.TerasolunaPlusApplicationBuilder;

/**
 * Main class to bootstrap and launch the Spring Boot-based TSF+ application
 */
public class Main {
	
	/**
	 * Private default constructor (non instanceable class)
	 */
	private Main() {
		super();
	}
	
	/**
	 * Bootstraps and launches the Spring Boot-based TSF+ application
	 * @param args optional command line arguments
	 */
	public static void main(String[] args) {
		
		new TerasolunaPlusApplicationBuilder().run(args);
	}
}

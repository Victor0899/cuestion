package com.everis.metajob.application.module.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@MappedSuperclass
public class AuditoriaModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "D_FECHA_CREACION")
	private Calendar fechaCreacion;

	@Column(name = "N_USUARIO_CREACION")
	private Long usuarioCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "D_FECHA_MODIFICACION")
	private Calendar fechaActualizacion;

	@Column(name = "N_USUARIO_MODIFICACION")
	private Long usuarioModificacion;

	@PrePersist
	protected void onCreate() {
		fechaCreacion = Calendar.getInstance();
	}

	@PreUpdate
	protected void onUpdate() {
		fechaActualizacion = Calendar.getInstance();
	}
}
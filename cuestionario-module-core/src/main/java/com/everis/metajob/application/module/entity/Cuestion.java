package com.everis.metajob.application.module.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ApiModel(value = "Cuestion")
@JsonPropertyOrder({ "contenido", "contextualizacion", "respuestasPermitiddas", "respuesta", "tipoRespuesta", "adjunto",
		"borrado" })

@Getter
@Setter
@Entity
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CUESTION")
public class Cuestion extends AuditoriaModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_CUESTION")
	private Long id;

	@Column(name = "V_CODIGO")
	private String codigo;

	@Column(name = "V_CONTEXTUALIZACION")
	private String contextualizacion;

	@Column(name = "V_CONTENIDO")
	private String contenido;

	@Column(name = "C_TIPO_RESPUESTA")
	private char tipoRespuesta;

	@Column(name = "I_RESPUESTAS_PERMITIDAS")
	private int respuestasPermitiddas;

	@Column(name = "B_BORRADO")
	private boolean borrado;

	@Column(name = "C_ADJUNTO")
	private char adjunto;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cuestion")
	private List<CuestionarioCuestion> cuestionariosCuestiones;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cuestion", cascade = CascadeType.ALL)
	private List<Respuesta> respuestas;

}

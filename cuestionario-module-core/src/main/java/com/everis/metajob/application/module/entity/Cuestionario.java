package com.everis.metajob.application.module.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CUESTIONARIO")
public class Cuestionario extends AuditoriaModel {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_CUESTIONARIO")
	private Long id;

	@Column(name = "V_CODIGO_CUESTIONARIO")
	private String codigoCuestionario;

	@Column(name = "V_NOMBRE")
	private String nombre;
	
	@Column(name = "V_DESCRIPCION")
	private String descripcion;

	@Column(name = "B_BORRADO")
	private boolean borrado;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cuestionario")
	private List<CuestionarioCuestion> cuestionariosCuestiones;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cuestionario")
	private List<CuestionarioDemandante> cuestionariosDemandantes;
	
}

package com.everis.metajob.application.module.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ApiModel(value = "CuestionarioCuestion")
@JsonPropertyOrder({ "id", "cuestion", "cuestionario", "obligatoria" })

@Getter
@Setter
@Entity
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CUESTIONARIO_CUESTION")
public class CuestionarioCuestion extends AuditoriaModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID")
	private Long id;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "N_ID_CUESTION")
	private Cuestion cuestion;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "N_ID_CUESTIONARIO")
	private Cuestionario cuestionario;

	@Column(name = "B_OBLIGATORIA")
	private boolean obligatoria;
}

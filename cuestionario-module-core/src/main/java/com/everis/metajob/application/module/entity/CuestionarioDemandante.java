package com.everis.metajob.application.module.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "CUESTIONARIO_DEMANDANTE")
public class CuestionarioDemandante {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "D_FECHA_ASIGNACION")
	private Calendar fechaAsignacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "D_FECHA_EJECUCION")
	private Calendar fechaEjecucion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "D_FECHA_CULMINACION")
	private Calendar fechaCulminacion;
	
	@Column(name = "C_ESTADO")
	private char estado;
	
	@Column(name = "N_ID_DEMANDANTE")
	private Long idDemandante;
	
	@Column(name = "N_ID_USUARIO")
	private Long idUsuario;
	
	@Column(name = "B_ACTIVO_VISIBLE")
	private boolean activoVisible;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CUESTIONARIO", referencedColumnName = "N_ID_CUESTIONARIO", foreignKey = @ForeignKey(name = "fk_cuestionariodemandante_cuestionario"))
	private Cuestionario cuestionario;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cuestionarioDemandante")
	private List<Resultado> resultados;
	
}

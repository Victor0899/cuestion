package com.everis.metajob.application.module.entity;

import javax.persistence.ForeignKey;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "RESULTADO")
public class Resultado {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID")
	private Long id;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "N_ID_RESPUESTA", referencedColumnName = "N_ID_RESPUESTA")
	private Respuesta respuesta;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "N_ID_CUESTIONARIO_DEMANDANTE", referencedColumnName = "N_ID", foreignKey = @ForeignKey(name = "fk_cuestionario_demandante_resultado"))
	private CuestionarioDemandante cuestionarioDemandante;		
}

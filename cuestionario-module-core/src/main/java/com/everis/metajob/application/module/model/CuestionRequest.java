package com.everis.metajob.application.module.model;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CuestionRequest {
	@ApiModelProperty(
			example = "CU1", 
			required = true, 
			value = "Código de la cuestión")
	@Size(min = 1, max = 50)
	@NotNull
	@NotEmpty
	private String codigo;
	
	@ApiModelProperty(
			example = "Indique su nivel de acuerdo con la siguiente afirmación", 
			required = false, 
			value = "Codigo de la cuestión")
	@Size(max = 300)
	private String contextualizacion;
	
	@ApiModelProperty(
			example = "En mis anteriores puestos de trabajo mis compañeros siempre han opinado que soy una persona de trato fácil.", 
			required = true, 
			value = "Contenido de la cuestión")
	@Size(min = 1, max = 300)
	@NotNull
	@NotEmpty
	private String contenido;
	
	@ApiModelProperty(
			example = "1", 
			required = true, 
			value = "Número de respuestas permitidas")
	@NotNull
	private int respuestasPermitiddas;
	
	@ApiModelProperty(
			example = "S", 
			required = true, 
			value = "Tipo de respuesta para la cuestión S(Singular)/M(Múltiple)")
	@NotNull
	private char tipoRespuesta;
	
	@ApiModelProperty(
			example = "/resources/Image.jpg", 
			required = true, 
			value = "Url de los archivos adjuntos")
	@Size(max = 500)
	private String adjunto;
	
	private List<RespuestaRequest> respuestas;

}

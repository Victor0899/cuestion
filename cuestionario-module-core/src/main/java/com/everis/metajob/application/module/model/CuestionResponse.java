package com.everis.metajob.application.module.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CuestionResponse {
	private Long id;
	private String codigo;
	private String contextualizacion;
	private String contenido;
	private int respuestasPermitiddas;
	private String respuesta;
	private String tipoRespuesta;
	private char adjunto;
	private boolean borrado;
	private List<RespuestaResponse> respuestas;

}

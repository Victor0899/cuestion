package com.everis.metajob.application.module.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespuestaRequest {
	private Long id;
	private String codigo;
	private String texto;
}

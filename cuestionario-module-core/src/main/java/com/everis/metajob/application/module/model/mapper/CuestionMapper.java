package com.everis.metajob.application.module.model.mapper;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;

import com.everis.metajob.application.module.entity.Cuestion;
import com.everis.metajob.application.module.model.CuestionRequest;
import com.everis.metajob.application.module.model.CuestionResponse;

public class CuestionMapper {
	public static List<CuestionResponse> mapForPaging(List<Cuestion> models) {
		ModelMapper mapper = new ModelMapper();

		mapper.addMappings(new PropertyMap<Cuestion, CuestionResponse>() {
			protected void configure() {

			}
		});

		return mapper.map(models, new TypeToken<List<CuestionResponse>>() {
		}.getType());
	}
	
	public static Cuestion mapForPaging(CuestionRequest model) {
		ModelMapper mapper = new ModelMapper();

		mapper.addMappings(new PropertyMap<CuestionRequest, Cuestion>() {
			protected void configure() {

			}
		});

		return mapper.map(model, new TypeToken<Cuestion>() {
		}.getType());
	}
	
	public static CuestionResponse mapForPaging(Cuestion model) {
		ModelMapper mapper = new ModelMapper();
		
		mapper.addMappings(new PropertyMap<Cuestion, CuestionResponse>() {
			protected void configure() {
				
			}
		});
		
		return mapper.map(model, new TypeToken<CuestionResponse>() {
		}.getType());
	}
}
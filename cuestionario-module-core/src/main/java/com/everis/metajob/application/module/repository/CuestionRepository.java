package com.everis.metajob.application.module.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.everis.metajob.application.module.entity.Cuestion;

@Repository
public interface CuestionRepository extends CrudRepository<Cuestion, Long> {

	@Modifying
	@Query("UPDATE Cuestion SET borrado = true WHERE id = ?1")
	@Transactional
	void desactivarCuestion(Long idCuestion);

	@Query("SELECT c FROM Cuestion c WHERE LOWER(concat(c.contenido,' ', c.contextualizacion)) LIKE %?1%")
	List<Cuestion> buscarPorContextoOContenido(String filter);
}

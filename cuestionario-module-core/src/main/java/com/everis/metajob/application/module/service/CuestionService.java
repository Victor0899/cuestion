package com.everis.metajob.application.module.service;

import java.util.List;

import org.terasoluna.gfw.common.exception.ResourceNotFoundException;

import com.everis.metajob.application.module.entity.Cuestion;

public interface CuestionService {
	
	public List<Cuestion> obtenerCuestiones();

	public Cuestion guardarCuestion(Cuestion cuestion);
	
	public Cuestion buscarPorId(Long id) throws ResourceNotFoundException;
	
	public boolean existeCuestion(Long id);
	
	public void desactivarCuestion(Long id);
	
	public List<Cuestion> buscarPorContextoOContenido(String filter);
	
}

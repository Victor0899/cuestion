package com.everis.metajob.application.module.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.terasoluna.gfw.common.exception.ResourceNotFoundException;

import com.everis.metajob.application.module.entity.Cuestion;
import com.everis.metajob.application.module.repository.CuestionRepository;
import com.everis.metajob.application.module.service.CuestionService;

@Service
public class CuestionServiceImpl implements CuestionService {

	@Autowired
	private CuestionRepository cuestionRepository;

	@Override
	public Cuestion guardarCuestion(Cuestion cuestion) {
		return cuestionRepository.save(cuestion);
	}

	@Override
	public Cuestion buscarPorId(Long id) {
		return cuestionRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("No se encontro la cuestion especificada"));
	}

	@Override
	public List<Cuestion> obtenerCuestiones() {
		return StreamSupport.stream(cuestionRepository.findAll().spliterator(), false)
				.filter(c -> !c.isBorrado())
				.collect(Collectors.toCollection(ArrayList::new));
	}

	@Override
	public boolean existeCuestion(Long id) {
		return cuestionRepository.existsById(id);
	}

	@Override
	public void desactivarCuestion(Long id) {
		cuestionRepository.desactivarCuestion(id);
	}

	@Override
	public List<Cuestion> buscarPorContextoOContenido(String filter) {
		return cuestionRepository.buscarPorContextoOContenido(filter.toLowerCase());
	}
}

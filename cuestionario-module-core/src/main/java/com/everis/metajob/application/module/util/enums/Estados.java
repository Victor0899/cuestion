package com.everis.metajob.application.module.util.enums;

public enum Estados {
    P {
        public String toString() {
            return "PENDIENTE";
        }
    },  
    C {
        public String toString() {
            return "EN CURSO";
        }
    },
    F {
        public String toString() {
            return "FINALIZADO";
        }
    }, 
    B {
    	public String toString() {
    		return "BLOQUEADO";
    	}
    } 
}
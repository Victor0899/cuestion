package com.everis.metajob.application.module.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.terasoluna.gfw.common.exception.ResourceNotFoundException;

import com.everis.metajob.application.module.exceptions.ErrorDetails;
import com.everis.metajob.application.module.model.CuestionRequest;
import com.everis.metajob.application.module.model.CuestionResponse;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

public interface CuestionarioController {

	@ApiOperation(value = "Devuelve todas las cuestiones.", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "GET")
	@ApiResponses({ 
			@ApiResponse(code = 200, message = "Operación exitosa.", response = CuestionResponse.class),
			@ApiResponse(code = 400, message = "La solicitud no pudo ser interpretada por una sintaxis inválida.", response = ErrorDetails.class),
			@ApiResponse(code = 404, message = "El servidor no pudo encontrar las cuestiones solicitadas.", response = ErrorDetails.class),
			@ApiResponse(code = 405, message = "El método especificado en la solicitud para el recurso identificado por la URI (Debe incluirse lista de metodos válidos).", response = ErrorDetails.class), })
	public ResponseEntity<List<CuestionResponse>> listarCuestiones();

	@ApiOperation(value = "Añadir una cuestión en el sistema.", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "POST")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Petición resuelta, se ha añadido la cuestión en la BBDD.", response = CuestionRequest.class),
			@ApiResponse(code = 400, message = "La solicitud no pudo ser interpretada por una sintaxis inválida.", response = ErrorDetails.class),
			@ApiResponse(code = 404, message = "El servidor no pudo encontrar la cuestión solicitada.", response = ErrorDetails.class),
			@ApiResponse(code = 405, message = "El método especificado en la solicitud para el recurso identificado por la URI (Debe incluirse lista de metodos válidos).", response = ErrorDetails.class), })
	public CuestionResponse crearCuestion(CuestionRequest cuestion);

	@ApiOperation(value = "Devuelve una única cuestion con ID {idCu}.", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "POST")
	@ApiResponses({ 
			@ApiResponse(code = 200, message = "Operación exitosa.", response = CuestionResponse.class),
			@ApiResponse(code = 400, message = "La solicitud no pudo ser interpretada por una sintaxis inválida.", response = ErrorDetails.class),
			@ApiResponse(code = 404, message = "El servidor no pudo encontrar la cuestion solicitada.", response = ErrorDetails.class),
			@ApiResponse(code = 405, message = "El método especificado en la solicitud para el recurso identificado por la URI (Debe incluirse lista de metodos válidos).", response = ErrorDetails.class), })
	public ResponseEntity<CuestionResponse> obtenerCuestion(long id) throws ResourceNotFoundException;

	@ApiOperation(value = "Modificar una cuestion que exista en el sistema.", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "PUT")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Se ha modificado la cuestion con id {idC}.", response = CuestionResponse.class),
			@ApiResponse(code = 400, message = "La solicitud no pudo ser interpretada por una sintaxis inválida.", response = ErrorDetails.class),
			@ApiResponse(code = 404, message = "El servidor no pudo encontrar la cuestión solicitada.", response = ErrorDetails.class),
			@ApiResponse(code = 405, message = "El método especificado en la solicitud para el recurso identificado por la URI (Debe incluirse lista de metodos válidos).", response = ErrorDetails.class), })
	public String actualizarCuestion(long id, CuestionRequest cuestion) throws ResourceNotFoundException;

	@ApiOperation(value = "Eliminado de un cuestion existente en el sistema.", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "DELETE")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Se ha eliminado la cuestion con id {id_cuestion}.", response = CuestionResponse.class),
			@ApiResponse(code = 400, message = "La solicitud no pudo ser interpretada por una sintaxis inválida.", response = ErrorDetails.class),
			@ApiResponse(code = 404, message = "El servidor no pudo encontrar la cuestión solicitada.", response = ErrorDetails.class),
			@ApiResponse(code = 405, message = "El método especificado en la solicitud para el recurso identificado por la URI (Debe incluirse lista de metodos válidos).", response = ErrorDetails.class), })
	public String eliminarCuestion(long id);

	@ApiOperation(value = "Operación exitosa.", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "GET")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Se ha eliminado la cuestion con id {id_cuestion}.", response = CuestionResponse.class),
			@ApiResponse(code = 400, message = "La solicitud no pudo ser interpretada por una sintaxis inválida.", response = ErrorDetails.class),
			@ApiResponse(code = 404, message = "El servidor no pudo encontrar la cuestión solicitada.", response = ErrorDetails.class),
			@ApiResponse(code = 405, message = "El método especificado en la solicitud para el recurso identificado por la URI (Debe incluirse lista de metodos válidos).", response = ErrorDetails.class), })
	public ResponseEntity<List<CuestionResponse>> listarCuestionesPorContextoOContenido(String filter);

}
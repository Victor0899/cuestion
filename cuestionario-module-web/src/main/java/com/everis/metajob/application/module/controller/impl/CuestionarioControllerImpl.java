package com.everis.metajob.application.module.controller.impl;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.terasoluna.gfw.common.exception.ResourceNotFoundException;

import com.everis.metajob.application.module.controller.CuestionarioController;
import com.everis.metajob.application.module.entity.Cuestion;
import com.everis.metajob.application.module.model.CuestionRequest;
import com.everis.metajob.application.module.model.CuestionResponse;
import com.everis.metajob.application.module.model.mapper.CuestionMapper;
import com.everis.metajob.application.module.service.CuestionService;

@RestController
public class CuestionarioControllerImpl implements CuestionarioController {

	@Autowired
	private CuestionService cuestionService;

	@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = "/cuestiones")
	public @ResponseBody ResponseEntity<List<CuestionResponse>> listarCuestiones() {
		List<Cuestion> cuestiones = cuestionService.obtenerCuestiones();
		return new ResponseEntity<List<CuestionResponse>>(CuestionMapper.mapForPaging(cuestiones), HttpStatus.OK);
	}

	@PostMapping(value = "/cuestiones")
	public CuestionResponse crearCuestion(@Valid @RequestBody CuestionRequest cuestion) {
		Cuestion c = cuestionService.guardarCuestion(CuestionMapper.mapForPaging(cuestion));
		return CuestionMapper.mapForPaging(c);
	}

	@GetMapping(value = "/cuestiones/{idCuestion}")
	public @ResponseBody ResponseEntity<CuestionResponse> obtenerCuestion(@PathVariable("idCuestion") @Min(1) long id)
			throws ResourceNotFoundException {
		Cuestion cuestion = cuestionService.buscarPorId(id);
		if (cuestion == null) {
			return new ResponseEntity<CuestionResponse>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<CuestionResponse>(CuestionMapper.mapForPaging(cuestion), HttpStatus.OK);
	}

	@PutMapping(value = "/cuestiones/{idCuestion}")
	public String actualizarCuestion(@PathVariable("idCuestion") @Min(1) long id,
			@Valid @RequestBody CuestionRequest cuestion) {
		if (cuestionService.existeCuestion(id)) {
			cuestionService.guardarCuestion(CuestionMapper.mapForPaging(cuestion));
			return "Se ha modificado la cuestion con id {" + id + "}";
		}
		return "Cuestión no encontrada";
	}

	@DeleteMapping(value = "/cuestiones/{idCuestion}")
	public String eliminarCuestion(@PathVariable("idCuestion") @Min(1) long id) throws ResourceNotFoundException {
		cuestionService.desactivarCuestion(id);
		return "Cuestión eliminada";
	}

	@GetMapping(value = "/cuestiones/buscar/{filter}")
	public @ResponseBody ResponseEntity<List<CuestionResponse>> listarCuestionesPorContextoOContenido(
			@PathVariable("filter") @Min(2) String filter) {
		List<Cuestion> cuestiones = cuestionService.buscarPorContextoOContenido(filter);
		return new ResponseEntity<List<CuestionResponse>>(CuestionMapper.mapForPaging(cuestiones), HttpStatus.OK);
	}

}
package com.everis.metajob.application.module.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CamposErroneos {

	private String mensaje;
	private String nombreCampo;
}

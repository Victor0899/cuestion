package com.everis.metajob.application.module.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ErrorDetailsFields {

	private final int estado;
    private final String mensaje;
    private List<CamposErroneos> campos = new ArrayList<>();

    public ErrorDetailsFields(int estado, String mensaje) {
        this.estado = estado;
        this.mensaje = mensaje;
    }

    public int getEstado() {
        return estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void agregarCampoError(String mensaje, String nombreCampo) {
    	CamposErroneos error = new CamposErroneos(mensaje, nombreCampo);
        campos.add(error);
    }

    public List<CamposErroneos> getCampos() {
        return campos;
    }

}
